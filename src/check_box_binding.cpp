#include "check_box_binding.h"
#include "qcheckbox.h"

#define WARNING(A,...)	{ printf("warning: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }
#define INFO(A,...)		{ printf("info: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }

CheckBoxBinding::CheckBoxBinding(QCheckBox* widget, const Value<bool>::PTR_T& paramValue)
	: WidgetBindingBase(widget)
	, m_checkBox(widget)
    , m_parameter(paramValue)
{

	m_onChangeCallback = [this](Value<bool>::PTR_T) {
		// this lambda is called if value has changed ....
		if (!m_suppressWidgetUpdate) {
			emit parameterChanged();
		}
	};
	m_parameter->addValueChangedCallback(&m_onChangeCallback);

	connect(this, SIGNAL(parameterChanged()), SLOT(onParameterChanged()), Qt::QueuedConnection);
	connect(widget, &QCheckBox::stateChanged, this, &CheckBoxBinding::onWidgetStateChanged);
}

CheckBoxBinding::~CheckBoxBinding()
{
	unbind();
}

CheckBoxBinding* CheckBoxBinding::bind(QCheckBox* widget, const Value<bool>::PTR_T& paramValue)
{
	return new CheckBoxBinding(widget, paramValue);
}

void CheckBoxBinding::unbind()
{
	if (m_parameter) {
		m_parameter->removeValueChangedCallback(&m_onChangeCallback);
		m_parameter.reset();
	}
}

void CheckBoxBinding::onWidgetStateChanged(int state)
{
	if (!m_parameter) {
		return;
	}

	// prevent endless update loop
	m_suppressWidgetUpdate = true;
	if (Qt::CheckState::Checked == state) {
		m_parameter->set(true);
	}
	else {
		m_parameter->set(false);
	}
	m_suppressWidgetUpdate = false;
}

void CheckBoxBinding::onParameterChanged()
{
	if (!m_parameter) {
		return;
	}

	m_checkBox->setChecked(m_parameter->get());
}
