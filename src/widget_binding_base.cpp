#include "widget_binding_base.h"
#include "qvariant.h"

WidgetBindingBase::WidgetBindingBase(QWidget* widget)
	: QObject(widget)
	, m_affinity(Affinity::UNKNOWN)
    , m_suppressWidgetUpdate(false)
{

}

WidgetBindingBase::~WidgetBindingBase()
{

}

void WidgetBindingBase::setAffinity(QWidget* wgt)
{
	QVariant prop = wgt->property("Affinity");

	if (prop.isValid()) {
		QString str = prop.toString();

		m_affinity = m_affinityMap[str.toStdString()];
	}
}

std::map<std::string, WidgetBindingBase::Affinity> WidgetBindingBase::m_affinityMap{
	std::make_pair("Path", WidgetBindingBase::Affinity::PATH),
	std::make_pair("FileIn", WidgetBindingBase::Affinity::FILE_IN),
	std::make_pair("FileOut", WidgetBindingBase::Affinity::FILE_OUT)
};

