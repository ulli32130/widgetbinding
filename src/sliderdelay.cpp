#include "sliderdelay.h"
#include "math.h"

SliderDelay::SliderDelay(WidgetBinding* param, QSlider* slider, float min, float max, uint32_t delay)
	: QObject(param)
{
	m_param			= param;
	m_slider		= slider;
	m_time			= delay;
	m_max			= max;
	m_min			= min;

	int range		= slider->maximum() - slider->minimum();
	m_multiplier	= (max - min) / range;
	m_offset		= min;

	m_suppressUpdate = false;

	connect(&m_delayTimer, &QTimer::timeout, this, &SliderDelay::onTimeout);
	connect(m_param, &WidgetBinding::valueChanged, this, &SliderDelay::onValueChanged);
	connect(m_slider, &QSlider::valueChanged, this, &SliderDelay::onSliderChanged);
	m_delayTimer.setSingleShot(true);
}

SliderDelay::~SliderDelay()
{

}

void SliderDelay::onSliderChanged()
{
	if (m_suppressUpdate) {
		return;
	}
	if (!m_delayTimer.isActive()) {
		m_delayTimer.start(m_time);
	}
}

void SliderDelay::onValueChanged()
{
	float pos = m_param->get().value<float>();

	pos = (pos / m_multiplier) - m_offset;

    int tickPos = roundf(pos);

	m_suppressUpdate = true;
	m_slider->setValue(tickPos);
	m_suppressUpdate = false;
}

void SliderDelay::onTimeout()
{
	int pos = m_slider->value();

	float val = (pos * m_multiplier) + m_offset;

	m_param->set(val, true, true, true);
}
