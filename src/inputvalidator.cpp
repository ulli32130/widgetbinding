#include "inputvalidator.h"


/*!
 * \brief 
 * Input validation of QString. Check if QString is
 * binary, decimal or hex represantation of a number
 * @param in 
 * \return QVariant converted value
 */
QVariant HexValidator::validate(QString & in)
{
	QString input = in.trimmed();
	bool ok;
	unsigned long value;

	input = input.toLower();
	if (input.startsWith("0x")) {
		value = input.toULong(&ok, 16);
		val.setValue(value);
	}
	else if (input.startsWith("0b")) {
		input = input.remove(0, 2);
		value = input.toULong(&ok, 2);
		val.setValue(value);
	}
	else {
		value = input.toULong(&ok, 10);
		val.setValue(value);
	}

	return val;
}
