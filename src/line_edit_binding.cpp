#include "line_edit_binding.h"
#include "qevent.h"
#include "qcheckbox.h"
#include "qfiledialog.h"

#define WARNING(A,...)	{ printf("warning: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }
#define INFO(A,...)		{ printf("info: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }

LineEditBinding::LineEditBinding(QLineEdit* widget, const ValueBase::PTR_T& paramValue)
	: WidgetBindingBase(widget)
	, m_lineEdit(widget)
    , m_parameter(paramValue)
	, m_fmtStr(NULL)
{

	setAffinity(widget);

	getFmtStrn();

	m_onChangeCallback = [this](ValueBase::PTR_T) {
		// this lambda is called if value has changed ....
		if (!m_suppressWidgetUpdate) {
			emit parameterChanged();
		}
	};
	m_parameter->addValueChangedCallback(&m_onChangeCallback);

	connect(this, SIGNAL(parameterChanged()), SLOT(onParameterChanged()), Qt::QueuedConnection);
	connect(widget, SIGNAL(returnPressed()), this, SLOT(onReturnPressed()));
	connect(widget, SIGNAL(editingFinished()), this, SLOT(onEditingFinished()));
	connect(widget, SIGNAL(textEdited(const QString&)), this, SLOT(onTextEdited(const QString&)));

	if (!m_lineEdit->isReadOnly()) {
		widget->installEventFilter(this);
	}

	onParameterChanged();
}

LineEditBinding::~LineEditBinding()
{
	unbind();
}

LineEditBinding* LineEditBinding::bind(QLineEdit* widget, const ValueBase::PTR_T& paramValue)
{
	return new LineEditBinding(widget, paramValue);
}

void LineEditBinding::unbind()
{
	if (m_parameter) {
		m_parameter->removeValueChangedCallback(&m_onChangeCallback);
		m_parameter.reset();
	}
}

bool LineEditBinding::eventFilter(QObject* obj, QEvent* event)
{
	if (!m_parameter) {
		return false;
	}

	if (event->type() == QEvent::Close) {
		obj->deleteLater();
		return true;
	}
		
	if (event->type() == QEvent::MouseButtonDblClick) {
		if (m_affinity == Affinity::PATH) {
			QString dialogCaption("Select path");
			QVariant caption = property("DialogCaption");
			if (caption.isValid()) {
				dialogCaption = caption.toString();
			}
			QString newPath = QFileDialog::getExistingDirectory(
				NULL, dialogCaption);

			if (newPath.length() != 0) {
				if (m_parameter != nullptr) {
					Value<std::string>::cast(m_parameter)->set(newPath.toStdString());
				}
				return true;
			}
		}
		else if (m_affinity == Affinity::FILE_IN) {
			QString dialogCaption("Select file");

			QVariant caption = property("DialogCaption");
			if (caption.isValid()) {
				dialogCaption = caption.toString();
			}

			QString path = "mops";   // todo: get current value

			if (!path.endsWith("/")) {
				
			}

			QString newFile = QFileDialog::getOpenFileName(
				nullptr, dialogCaption, path);

			if (newFile.length() != 0) {
				Value<std::string>::cast(m_parameter)->set(newFile.toStdString());
				return true;
			}
		}
		else if (m_affinity == Affinity::FILE_OUT) {

			QString dialogCaption("Select file");

			QVariant caption = property("DialogCaption");
			if (caption.isValid()) {
				dialogCaption = caption.toString();
			}

			QString newFile = QFileDialog::getSaveFileName(
				NULL, dialogCaption);

			if (newFile.length() != 0) {
				Value<std::string>::cast(m_parameter)->set(newFile.toStdString());
				return true;
			}
		}
	}

	return false;
}

void LineEditBinding::getFmtStrn()
{
	QVariant qvFmtStng = m_lineEdit->property("FormatString");
	if (qvFmtStng.isValid()) {
		m_fmtQStr = qvFmtStng.toString();
	}
}

bool LineEditBinding::setNewValue(QString& input)
{
    (void) input;

	return true;
}


void LineEditBinding::onParameterChanged()
{
	if (!m_parameter) {
		return;
	}

	m_suppressWidgetUpdate = true;

	switch (m_parameter->getValueType()) {
	case VALUE_TYPE::INT8:
	{
		int8_t val = std::dynamic_pointer_cast<Value<int8_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%d", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::UINT8:
	{
		uint8_t val = std::dynamic_pointer_cast<Value<uint8_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%d", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::INT16:
	{
		int16_t val = std::dynamic_pointer_cast<Value<int16_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%d", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::UINT16:
	{
		uint16_t val = std::dynamic_pointer_cast<Value<uint16_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%u", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::INT32:
	{
		int32_t val = std::dynamic_pointer_cast<Value<int32_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%d", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::UINT32:
	{
		uint32_t val = std::dynamic_pointer_cast<Value<uint32_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%u", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::INT64:
	{
		int64_t val = std::dynamic_pointer_cast<Value<int64_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
            str = QString().asprintf("%lu", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::UINT64:
	{
		uint64_t val = std::dynamic_pointer_cast<Value<int64_t>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
            str = QString().asprintf("%lu", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::FLOAT:
	{
		float val = std::dynamic_pointer_cast<Value<float>>(m_parameter)->get();

		QString str;

		if (m_fmtQStr.length()) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%f", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::DOUBLE:
	{
		double val = std::dynamic_pointer_cast<Value<double>>(m_parameter)->get();

		QString str;

		if (m_fmtStr != NULL) {
			str = QString().asprintf(m_fmtQStr.toStdString().c_str(), val);
		}
		else {
			str = QString().asprintf("%f", val);
		}
		m_lineEdit->setText(str);
	}
	break;

	case VALUE_TYPE::BOOL:
		break;

	case VALUE_TYPE::STRING:
	{
		std::string val = std::dynamic_pointer_cast<Value<std::string>>(m_parameter)->get();
		m_lineEdit->setText(QString(val.c_str()));
		break;
	}

	default:
		break;
	}

	m_suppressWidgetUpdate = false;
}

void LineEditBinding::onReturnPressed()
{

}

void LineEditBinding::onEditingFinished()
{
	if (!m_parameter) {
		return;
	}

	QString input = m_lineEdit->text();

	bool ok = false;;

	m_suppressWidgetUpdate = true;

	switch (m_parameter->getValueType()) {
	case VALUE_TYPE::INT8:
	{
		uint8_t val;

		val = input.toInt(&ok);
		if(ok)
			ok = std::dynamic_pointer_cast<Value<int8_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::UINT8:
	{
		uint8_t val;

		val = input.toUInt(&ok);
		if(ok) 
			ok = std::dynamic_pointer_cast<Value<uint8_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::INT16:
	{
		int16_t val;
		
		val = input.toInt(&ok);
		if(ok)
			ok = std::dynamic_pointer_cast<Value<int16_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::UINT16:
	{
		uint16_t val;
	
		val = input.toUInt(&ok);
		if(ok)
			ok = std::dynamic_pointer_cast<Value<uint16_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::INT32:
	{
		int32_t val;
		
		val = input.toInt(&ok);
		if (ok)
			ok = std::dynamic_pointer_cast<Value<int32_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::UINT32:
	{
		uint32_t val;
	
		val = input.toUInt(&ok);
		if (ok)
			ok = std::dynamic_pointer_cast<Value<uint32_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::INT64:
	{
		int64_t val;

		val = input.toLong(&ok);
		if (ok)
			ok = std::dynamic_pointer_cast<Value<int64_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::UINT64:
	{
		uint64_t val;
	
		val = input.toULong(&ok);
		if (ok)
			ok = std::dynamic_pointer_cast<Value<uint64_t>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::FLOAT:
	{
		float val;

		val = input.toFloat(&ok);
		if (ok)
			ok = std::dynamic_pointer_cast<Value<float>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::DOUBLE:
	{
		double val;
	
		val = input.toDouble(&ok);
		if (ok)
			ok = std::dynamic_pointer_cast<Value<double>>(m_parameter)->set(val);
	}
	break;
	case VALUE_TYPE::BOOL:

		break;

	case VALUE_TYPE::STRING:
		ok = true;
		std::dynamic_pointer_cast<Value<std::string>>(m_parameter)->set(input.toStdString());
		break;
	default:
		break;
	}

	if (!ok) {
		m_lineEdit->setText("Err!");
	}

	m_suppressWidgetUpdate = false;
}

void LineEditBinding::onTextEdited(const QString& txt)
{

}
