#include "combo_box_binding.h"
#include "qcombobox.h"

ComboBoxBinding::ComboBoxBinding(QComboBox* widget,const ValueSelection::PTR_T& paramValue)
	: WidgetBindingBase(widget)
	, m_comboBox(widget)
    , m_parameter(paramValue)
{
	m_suppressWidgetUpdate = false;

	m_onSelectionChanged = [this](ValueSelection::PTR_T) {
		if (!m_suppressWidgetUpdate) {
			emit parameterChanged();
		}
	};

	m_parameter->addListener(&m_onSelectionChanged);

	connect(this, SIGNAL(parameterChanged()), SLOT(onParameterChanged()), Qt::QueuedConnection);
	connect(m_comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(onCurrentIndexChanged(int)));

	m_onListChanged = [this](ValueSelection::PTR_T p) {
		emit listChanged();
	};
	m_parameter->addListWatcher(&m_onListChanged);

	connect(this, SIGNAL(listChanged()), SLOT(onListChanged()));

	setComboItems();
	int idx = m_parameter->getSelectedIdx();
	m_comboBox->setCurrentIndex(idx);
}

ComboBoxBinding::~ComboBoxBinding()
{
	unbind();
}

ComboBoxBinding* ComboBoxBinding::bind(QComboBox* widget, const ValueSelection::PTR_T& paramValue)
{
	return new ComboBoxBinding(widget, paramValue);
}

void ComboBoxBinding::setComboItems()
{
	std::vector<std::string>entrys = m_parameter->getItems();

	m_suppressWidgetUpdate = true;

	m_comboBox->clear();

	for (std::vector<std::string>::iterator it = entrys.begin(); it != entrys.end(); it++) {
		m_comboBox->addItem(QString((*it).c_str()));
	}

	m_suppressWidgetUpdate = false;
}

void ComboBoxBinding::unbind()
{
	if (m_parameter) {
		m_parameter->removeListWatcher(&m_onListChanged);
		m_parameter->removeListener(&m_onSelectionChanged);

		m_parameter.reset();
	}
}

void ComboBoxBinding::onParameterChanged()
{
	m_suppressWidgetUpdate = true;
	// signal triggered by parameter change	
	if(m_parameter)
		m_comboBox->setCurrentIndex(m_parameter->getSelectedIdx());
	m_suppressWidgetUpdate = false;
}

void ComboBoxBinding::onCurrentIndexChanged(int n)
{
	// signal triggered by widget
	if (!m_suppressWidgetUpdate) {
		if(m_parameter)
			m_parameter->setSelectedIdx(n);
	}
}

void ComboBoxBinding::onListChanged()
{
	setComboItems();
}
