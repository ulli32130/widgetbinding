#include "lcd_binding.h"
#include "qlcdnumber.h"

#define WARNING(A,...)	{ printf("warning: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }
#define INFO(A,...)		{ printf("info: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }

LcdBinding::LcdBinding(QLCDNumber* widget, const ValueBase::PTR_T& paramValue)
	: WidgetBindingBase(widget)
	, m_lcdWidget(widget)
    , m_parameter(paramValue)
{

	m_onChangeCallback = [this](ValueBase::PTR_T) {
		// this lambda is called if value has changed ....
		if (!m_suppressWidgetUpdate) {
			emit parameterChanged();
		}
	};
	m_parameter->addValueChangedCallback(&m_onChangeCallback);

	connect(this, SIGNAL(parameterChanged()), SLOT(onParameterChanged()), Qt::QueuedConnection);
	onParameterChanged();
}

LcdBinding::~LcdBinding()
{
	unbind();
}

LcdBinding* LcdBinding::bind(QLCDNumber* widget, const ValueBase::PTR_T& paramValue)
{
	return new LcdBinding(widget, paramValue);
}

void LcdBinding::unbind()
{
	if (m_parameter) {
		m_parameter->removeValueChangedCallback(&m_onChangeCallback);

		m_parameter.reset();
	}
}

void LcdBinding::onParameterChanged()
{
	if (!m_parameter) {
		return;
	}

	switch (m_parameter->getValueType()) {
	case VALUE_TYPE::INT32:
		m_lcdWidget->display(std::dynamic_pointer_cast<Value<int32_t>>(m_parameter)->get());
		break;

	case VALUE_TYPE::UINT32:
		m_lcdWidget->display(static_cast<int32_t>(std::dynamic_pointer_cast<Value<uint32_t>>(m_parameter)->get()));
		break;

	case VALUE_TYPE::INT16:
		m_lcdWidget->display(static_cast<int32_t>(std::dynamic_pointer_cast<Value<int16_t>>(m_parameter)->get()));
		break;

	case VALUE_TYPE::UINT16:
		m_lcdWidget->display(static_cast<int32_t>(std::dynamic_pointer_cast<Value<uint16_t>>(m_parameter)->get()));
		break;

	case VALUE_TYPE::FLOAT:
		m_lcdWidget->display(static_cast<double>(std::dynamic_pointer_cast<Value<float>>(m_parameter)->get()));
		break;

	case VALUE_TYPE::DOUBLE:
		m_lcdWidget->display(std::dynamic_pointer_cast<Value<double>>(m_parameter)->get());
		break;

	default:
		m_lcdWidget->display("Unsupported binding !!");
	}
}
