#include "qstringlisteditor.h"
#include "qtextdocument.h"


QStringListEditor::QStringListEditor(QWidget *parent, QStringList& sList)
	: QWidget(parent)
{
	ui.setupUi(this);

	foreach(QString s, sList) {
		ui.plainTextEdit->appendPlainText(s);
	}
}

QStringListEditor::~QStringListEditor()
{
	


}

void QStringListEditor::onSave()
{
	QStringList l = ui.plainTextEdit->toPlainText().split("\n");

	listChanged(l);
	close();
}

void QStringListEditor::onAbort()
{
	close();
}
