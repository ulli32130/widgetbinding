#include "label_binding.h"
#include "qlabel.h"


LabelBinding::LabelBinding(QLabel* widget, const ValueBase::PTR_T& paramValue)
	: WidgetBindingBase(widget)
	, m_label(widget)
    , m_parameter(paramValue)
{

	m_onChangeCallback = [this](const ValueBase::PTR_T& p) {
		// this lambda is called if value has changed ....
		if (!m_suppressWidgetUpdate) {
			emit parameterChanged();
		}
	};
	m_parameter->addValueChangedCallback(&m_onChangeCallback);

	connect(this, SIGNAL(parameterChanged()), SLOT(onParameterChanged()), Qt::QueuedConnection);
}

LabelBinding::~LabelBinding()
{
	unbind();
}

LabelBinding* LabelBinding::bind(QLabel* widget, const ValueBase::PTR_T& paramValue)
{
	return new LabelBinding(widget, paramValue);
}

void LabelBinding::unbind()
{
	if (m_parameter) {
		m_parameter->removeValueChangedCallback(&m_onChangeCallback);

		m_parameter.reset();
	}
}

void LabelBinding::onWidgetStateChanged(int state)
{
	if (!m_parameter) {
		return;
	}

	// prevent endless update loop
	m_suppressWidgetUpdate = true;
	
	// TODO: do the work

	m_suppressWidgetUpdate = false;
}

void LabelBinding::onParameterChanged()
{
	if (!m_parameter) {
		return;
	}

	// not yet implemented .....
	// TODO: do the work
}
