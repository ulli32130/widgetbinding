#include "slider_binding.h"
#include "qlabel.h"

#define WARNING(A,...)	{ printf("warning: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }
#define INFO(A,...)		{ printf("info: %s: %d:", __FILE__, __LINE__); printf(A,##__VA_ARGS__); }


SliderBinding::SliderBinding(QSlider* widget, const ValueBase::PTR_T& paramValue)
	: WidgetBindingBase(widget)
	, m_slider(widget)
    , m_parameter(paramValue)
	, m_changeDelay(0)
{

	m_onChangeCallback = [this](ValueBase::PTR_T) {
		// this lambda is called if value has changed ....
		if (!m_suppressWidgetUpdate) {
			emit parameterChanged();
		}
	};

	m_parameter->addValueChangedCallback(&m_onChangeCallback);

	if (paramValue->hasRange()) {
		int min, max;
		if (getRange(min, max)) {
			m_slider->setMinimum(min);
			m_slider->setMaximum(max);
		}
	}

	connect(this, SIGNAL(parameterChanged()), this, SLOT(onParameterChanged()), Qt::QueuedConnection);
	connect(m_slider, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));

	connect(&m_delayTimer, &QTimer::timeout, this, &SliderBinding::onTimeout);

	m_slider->setValue(getValue(m_parameter));
}

void SliderBinding::setDelay(uint32_t msDelay)
{
	m_changeDelay = msDelay;
	m_delayTimer.setSingleShot(true);
	m_delayTimer.setInterval(m_changeDelay);
}

void SliderBinding::unbind()
{
	if (m_parameter) {
		m_parameter->removeValueChangedCallback(&m_onChangeCallback);

		m_parameter.reset();
	}
}

bool SliderBinding::getRange(int& min, int& max)
{
	if (!m_parameter) {
		return false;
	}

	if (!m_parameter->hasRange()) {
		return false;
	}

	switch (m_parameter->getValueType()) {
	case VALUE_TYPE::INT8:
		min = std::dynamic_pointer_cast<Value<int8_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<int8_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::UINT8:
		min = std::dynamic_pointer_cast<Value<uint8_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<uint8_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::INT16:
		min = std::dynamic_pointer_cast<Value<int16_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<int16_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::UINT16:
		min = std::dynamic_pointer_cast<Value<uint16_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<uint16_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::INT32:
		min = std::dynamic_pointer_cast<Value<int32_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<int32_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::UINT32:
		min = std::dynamic_pointer_cast<Value<uint32_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<uint32_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::INT64:
		min = std::dynamic_pointer_cast<Value<int64_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<int64_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::UINT64:
		min = std::dynamic_pointer_cast<Value<uint64_t>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<uint64_t>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::FLOAT:
		min = std::dynamic_pointer_cast<Value<float>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<float>>(m_parameter)->getMax();
		return true;

	case VALUE_TYPE::DOUBLE:
		min = std::dynamic_pointer_cast<Value<double>>(m_parameter)->getMin();
		max = std::dynamic_pointer_cast<Value<double>>(m_parameter)->getMax();
		return true;

	default:
		min = 0;
		max = 0;
		return false;
	}
}

void SliderBinding::onTimeout()
{
	setValueFromGui(m_currentValue);
}

int SliderBinding::getValue(ValueBase::PTR_T p)
{
	if (!m_parameter) {
		return 0;
	}

	switch (p->getValueType()) {
	case VALUE_TYPE::INT8:
		return std::dynamic_pointer_cast<Value<int8_t>>(p)->get();
	case VALUE_TYPE::UINT8:
		return std::dynamic_pointer_cast<Value<uint8_t>>(p)->get();
	case VALUE_TYPE::INT16:
		return std::dynamic_pointer_cast<Value<int16_t>>(p)->get();
	case VALUE_TYPE::UINT16:
		return std::dynamic_pointer_cast<Value<uint16_t>>(p)->get();
	case VALUE_TYPE::INT32:
		return std::dynamic_pointer_cast<Value<int32_t>>(p)->get();
	case VALUE_TYPE::UINT32:
		return std::dynamic_pointer_cast<Value<uint32_t>>(p)->get();
	case VALUE_TYPE::INT64:
		return std::dynamic_pointer_cast<Value<int64_t>>(p)->get();
	case VALUE_TYPE::UINT64:
		return std::dynamic_pointer_cast<Value<uint64_t>>(p)->get();
	case VALUE_TYPE::FLOAT:
		return std::dynamic_pointer_cast<Value<float>>(p)->get();
	case VALUE_TYPE::DOUBLE:
		return std::dynamic_pointer_cast<Value<double>>(p)->get();
	default:
		return 0;
		break;
	}
}

void SliderBinding::setValueFromGui(int val)
{
	if (!m_parameter) {
		return;
	}

	// prevent endless update loop
	m_suppressWidgetUpdate = true;

	switch (m_parameter->getValueType()) {
	case VALUE_TYPE::INT8:
		std::dynamic_pointer_cast<Value<int8_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::UINT8:
		std::dynamic_pointer_cast<Value<uint8_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::INT16:
		std::dynamic_pointer_cast<Value<int16_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::UINT16:
		std::dynamic_pointer_cast<Value<uint16_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::INT32:
		std::dynamic_pointer_cast<Value<int32_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::UINT32:
		std::dynamic_pointer_cast<Value<uint32_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::INT64:
		std::dynamic_pointer_cast<Value<int64_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::UINT64:
		std::dynamic_pointer_cast<Value<uint64_t>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::FLOAT:
		std::dynamic_pointer_cast<Value<float>>(m_parameter)->set(val);
		break;
	case VALUE_TYPE::DOUBLE:
		std::dynamic_pointer_cast<Value<double>>(m_parameter)->set(val);
		break;
	default:
		break;
	}

	m_suppressWidgetUpdate = false;
}

void SliderBinding::onValueChanged(int val)
{
	m_currentValue = val;

	if (m_changeDelay != 0) {
		m_delayTimer.setInterval(m_changeDelay);
		m_delayTimer.start();
	}
	else {
		setValueFromGui(val);
	}

}

void SliderBinding::onParameterChanged()
{
	if (!m_parameter) {
		return;
	}

	m_slider->setValue(getValue(m_parameter));
}
