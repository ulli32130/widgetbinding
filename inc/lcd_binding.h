#pragma once

#include "parameter_value.h"
#include "qlcdnumber.h"
#include "widget_binding_base.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class WGTBINDING_EXPORT LcdBinding : public WidgetBindingBase
{

	Q_OBJECT

public:
	LcdBinding(QLCDNumber* widget, const ValueBase::PTR_T& paramValue);
	~LcdBinding();

	static LcdBinding* bind(QLCDNumber* widget, const ValueBase::PTR_T& paramValue);

	void unbind()									override;

signals:
	void parameterChanged();

public slots:
	void onParameterChanged();

private:
	std::function<void(ValueBase::PTR_T)>		m_onChangeCallback;
	std::function<void(ValueBase::PTR_T)>		m_onValueDestroyed;
	QLCDNumber*									m_lcdWidget;
	ValueBase::PTR_T							m_parameter;
};
