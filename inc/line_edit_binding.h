#pragma once

#include "parameter_value.h"
#include "qlineedit.h"
#include "widget_binding_base.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class WGTBINDING_EXPORT LineEditBinding : public WidgetBindingBase
{

	Q_OBJECT

public:
	LineEditBinding(QLineEdit* widget, const ValueBase::PTR_T& paramValue);
	~LineEditBinding();

	static LineEditBinding* bind(QLineEdit* widget, const ValueBase::PTR_T& paramValue);

	bool eventFilter(QObject* obj, QEvent* event)	override;
	void unbind()									override;

signals:
	void parameterChanged();

public slots:
	void onParameterChanged();

	void onReturnPressed();
	void onEditingFinished();
	void onTextEdited(const QString& txt);

private:
	std::function<void(ValueBase::PTR_T)>		m_onValueDestroyed;
	std::function<void(ValueBase::PTR_T)>		m_onChangeCallback;
	QLineEdit*									m_lineEdit;

	ValueBase::PTR_T							m_parameter;
	QString										m_fmtQStr;
	const char*									m_fmtStr;

	void getFmtStrn();

	bool setNewValue(QString& input);
};
