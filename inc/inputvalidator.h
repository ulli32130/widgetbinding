#pragma once

#include <QObject>
#include "qvariant.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class WGTBINDING_EXPORT InputValidator : public QObject
{
	Q_OBJECT

public:
	InputValidator(QObject *parent):QObject(parent) {};
	~InputValidator() {};

	virtual QVariant validate(QString& in) = 0;
    //virtual QVariant validate(QString* in) = 0;

protected:
	QVariant val;
};

#ifdef __UNUSED__

class WGTBINDING_EXPORT HexValidator : public InputValidator
{
	Q_OBJECT

public:
	HexValidator(QObject *parent):InputValidator(parent) {};
	~HexValidator() {};

	QVariant validate(QString& in);
};

#endif
