#pragma once

#include <QObject>
#include "QTimer.h"
#include "qslider.h"
#include "widgetBinding.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class WGTBINDING_EXPORT SliderDelay : public QObject
{
	Q_OBJECT

private:
	QTimer				m_delayTimer;
	WidgetBinding*		m_param;
	QSlider*			m_slider;
	uint32_t			m_time;
	float				m_min;
	float				m_max;
	float				m_multiplier;
	float				m_offset;
	bool				m_suppressUpdate;

public:
	SliderDelay(WidgetBinding* param, QSlider* slider, float min, float max, uint32_t delay);
	~SliderDelay();


public slots:
	void onSliderChanged();
	void onValueChanged();
	void onTimeout();
};
