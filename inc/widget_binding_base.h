#pragma once

#include <QObject>
#include "qwidget.h"

#include <map>

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class WGTBINDING_EXPORT WidgetBindingBase : public QObject
{
	Q_OBJECT
protected:

public:
	enum class Affinity : int {
		UNKNOWN,
		PATH,
		FILE_IN,
		FILE_OUT
	};

public:

	WidgetBindingBase(QWidget* widget);
	~WidgetBindingBase();

	
	virtual void unbind() = 0;

protected:
	void										setAffinity(QWidget* wgt);
	Affinity									m_affinity;
	bool										m_suppressWidgetUpdate;

	static std::map<std::string, Affinity>		m_affinityMap;
};