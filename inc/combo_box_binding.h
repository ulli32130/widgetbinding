#pragma once

#include "parameter_value.h"
#include "qcombobox.h"
#include "widget_binding_base.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class  WGTBINDING_EXPORT ComboBoxBinding : public WidgetBindingBase
{

	Q_OBJECT

public:
	ComboBoxBinding(QComboBox* widget, const ValueSelection::PTR_T& paramValue);

	~ComboBoxBinding();

	static ComboBoxBinding* bind(QComboBox* widget, const ValueSelection::PTR_T& paramValue);

	void unbind() override;

	void setComboItems();

signals:
	void parameterChanged();
	void listChanged();

public slots:
	void onListChanged();
	void onParameterChanged();
	void onCurrentIndexChanged(int);

private:
	std::function<void(ValueSelection::PTR_T)>			m_onListChanged;
	std::function<void(ValueSelection::PTR_T)>			m_onSelectionChanged;
	std::function<void(ValueBase::PTR_T)>				m_onValueDestroyed;
	QComboBox*											m_comboBox;
	ValueSelection::PTR_T								m_parameter;
};
