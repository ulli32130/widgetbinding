#pragma once

#include "parameter_value.h"
#include "qlabel.h"
#include "widget_binding_base.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class WGTBINDING_EXPORT LabelBinding : public WidgetBindingBase
{

	Q_OBJECT

public:
	LabelBinding(QLabel* widget, const ValueBase::PTR_T& paramValue);
	~LabelBinding();

	static LabelBinding* bind(QLabel* widget, const ValueBase::PTR_T& paramValue);

	void unbind()									override;

signals:
	void parameterChanged();

public slots:
	void onParameterChanged();
	void onWidgetStateChanged(int state);

private:
	std::function<void(ValueBase::PTR_T)>	m_onChangeCallback;
	QLabel*									m_label;
	ValueBase::PTR_T						m_parameter;
};
