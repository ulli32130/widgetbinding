#pragma once

#include "parameter_value.h"
#include "qcheckbox.h"
#include "widget_binding_base.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif    

class WGTBINDING_EXPORT CheckBoxBinding : public WidgetBindingBase
{

	Q_OBJECT

public:
	CheckBoxBinding(QCheckBox* widget, const Value<bool>::PTR_T& paramValue);
	~CheckBoxBinding();

	static CheckBoxBinding* bind(QCheckBox* widget, const Value<bool>::PTR_T& paramValue);

	void unbind()									override;

signals:
	void parameterChanged();

public slots:
	void onParameterChanged();
	void onWidgetStateChanged(int state);

private:
	std::function<void(std::shared_ptr<Value<bool>>)>				m_onChangeCallback;
	std::function<void(std::shared_ptr<ValueBase>)>					m_onValueDestroyed;
	QCheckBox*														m_checkBox;
	std::shared_ptr<Value<bool>>									m_parameter;
};
