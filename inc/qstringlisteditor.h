#pragma once

#ifdef __UNUSED__

#include <QWidget>
//#include "ui/ui_qstringlisteditor.h"

#include "qstringlist.h"

class QStringListEditor : public QWidget
{
	Q_OBJECT

public:
	QStringListEditor(QWidget *parent, QStringList& slist);
	~QStringListEditor();

public slots:
	void onSave();
	void onAbort();

signals:
	void listChanged(QStringList& newList);

private:
	//Ui::QStringListEditor ui;
};

#endif