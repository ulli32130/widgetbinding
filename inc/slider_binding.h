#pragma once

#include "parameter_value.h"
#include "qslider.h"
#include "qtimer.h"
#include "widget_binding_base.h"

#ifdef DYNAMIC_LIB
    #include "wgtbinding_global.h"
#else    
    #define WGTBINDING_EXPORT
#endif   

class  WGTBINDING_EXPORT SliderBinding : public WidgetBindingBase
{

	Q_OBJECT

public:
	SliderBinding(QSlider* widget, const ValueBase::PTR_T& paramValue);

	~SliderBinding() {
		unbind();
	}

	static SliderBinding* bind(QSlider* widget, const ValueBase::PTR_T& paramValue)
	{
		return new SliderBinding(widget, paramValue);
	}

	void unbind() override;

	void setDelay(uint32_t msDelay);

	int getValue(std::shared_ptr<ValueBase> p);
	bool getRange(int& min, int& max);

	void setValueFromGui(int);

signals:
	void parameterChanged();

public slots:
	void onParameterChanged();
	void onValueChanged(int);

private slots:
	void onTimeout();

private:
	std::function<void(ValueBase::PTR_T)>					m_onChangeCallback;
	std::function<void(ValueBase::PTR_T)>					m_onValueDestroyed;
	QSlider*												m_slider;
	ValueBase::PTR_T										m_parameter;

	uint32_t												m_changeDelay;
	QTimer													m_delayTimer;
	int														m_currentValue;
};
